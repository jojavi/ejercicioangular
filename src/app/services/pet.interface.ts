export interface Pet{
    uuId: string;
    race: string;
    name: string;
    age: number;
    notes: Note[] | null;
    createdAt: Date | any;
    createdBy : string;
    status: boolean;
}

export interface Note{
    note: string;
}