import { Injectable } from '@angular/core';
import { AngularFirestore} from '@angular/fire/compat/firestore';
import { Owner } from './owner.interface';

@Injectable({
  providedIn: 'root'
})
export class OwnerService {

  constructor(private firestore: AngularFirestore) { }

  getOwners(){
    return this.firestore.collection('owner').snapshotChanges();

  }
  addOwner(owner:Owner){
    return this.firestore.collection('owner').add(owner);
  }

  updateOwner(owner:Owner){
    return this.firestore.doc(owner.uuId).update(owner);
  }

}
