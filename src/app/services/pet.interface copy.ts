export interface Owner{
    uuId: string;
    dpi: string;
    name: string;
    lastname: string;
    age: number;
    createdAt: Date | any;
    createdBy : string;
    status: boolean;
}

