import { LEADING_TRIVIA_CHARS } from '@angular/compiler/src/render3/view/template';
import { Injectable } from '@angular/core';
import { AngularFirestore} from '@angular/fire/compat/firestore';
import { Pet } from './pet.interface';
@Injectable({
  providedIn: 'root'
})
export class PetService {

  constructor(private firestore: AngularFirestore) { }

  getPets(){
    return this.firestore.collection('pet').snapshotChanges();

  }
  addPet(pet:Pet){
    return this.firestore.collection('pet').add(pet);
  }

  updatePet(pet:Pet){
    return this.firestore.doc(pet.uuId).update(pet);
  }



}
