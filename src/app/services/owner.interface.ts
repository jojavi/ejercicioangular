export interface Owner{
    uuId: string;
    dpi: number;
    name: string;
    lastname: string;
    createdAt: Date | any;
    createdBy : string;
    status: boolean;
}
