import { Component, OnInit } from '@angular/core';
import { Owner } from '../services/owner.interface';
import { OwnerService } from '../services/owner.service';

@Component({
  selector: 'app-owner',
  templateUrl: './owner.component.html',
  styleUrls: ['./owner.component.css']
})
export class OwnerComponent implements OnInit {
  owner: Owner;
  
  owners: Owner[];
  constructor(private ownerService: OwnerService) {
    this.owner = {} as Owner;
    this.owners = [] as Owner[];
 }

  ngOnInit(): void {
    this.ownerService.getOwners().subscribe(data =>{
      this.owners = [];
      data.forEach(owner => {
        console.log('data: ', owner.payload.doc.data())
        this.owners.push(owner.payload.doc.data() as Owner)
      });
    })
  }

  createOwner(){
    
    this.owner.createdBy = "Javier";
    this.owner.createdAt = Date();
    this.owner.status = true;
    this.ownerService.addOwner(this.owner).then((() => {
      alert("El cliente fue registrado");

    }))
    
    console.log('Owner: ', this.owner)
  }

}
