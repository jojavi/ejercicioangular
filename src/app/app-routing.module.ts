import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PetComponent } from './pet/pet.component';
import { OwnerComponent } from './owner/owner.component';


const routes: Routes = [
  {path: 'pet' , component: PetComponent},
  {path: 'owner' , component: OwnerComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
