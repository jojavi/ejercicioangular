// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAnujtmlC5UDOABAucqCEHTJNvOHFRd_yE",
    authDomain: "snap-pet-180c8.firebaseapp.com",
    projectId: "snap-pet-180c8",
    storageBucket: "snap-pet-180c8.appspot.com",
    messagingSenderId: "1082338055292",
    appId: "1:1082338055292:web:ba6f44374cb08f7eb265f9",
    measurementId: "G-X2PPZX7XR5"
    }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
